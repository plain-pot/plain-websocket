import {connection, IServerConfig, server as WebSocketServer} from 'websocket';
import {iWssClientMessageExpander, iWssClientMessageMeta, iWssConnectionListener, iWssServerMessageMeta} from "../utils/pws.type";

/**
 * 每个连接自动创建的信息类型
 * @author  韦胜健
 * @date    2023/10/17 18:04
 */
export interface iWssConnectionMeta {
  /*websocket connection连接对象*/
  connection: connection,
  /*本次连接申请得到的名称（id唯一标识）*/
  name: string,
  /*是否暂定这个链接的通信*/
  pause: boolean,
  /*连接监听的消息*/
  listener: iWssConnectionListener[]
}

/**
 * 创建服务端websocket服务
 * @author  韦胜健
 * @date    2023/10/18 15:53
 */
export function createServerPss(createConfig: {
  /**
   * 创建Websocket实例时构造函数的参数
   * @author  韦胜健
   * @date    2023/10/18 15:39
   */
  wsParameters: IServerConfig,
  /**
   * 判断来源是有有效
   * @author  韦胜健
   * @date    2023/10/18 15:40
   */
  originIsAllowed: (origin: string) => boolean,
  /**
   * 处理来自客户端的消息
   * @author  韦胜健
   * @date    2023/10/18 15:40
   */
  messageHandler: { [k in keyof iWssClientMessageExpander]?: (data: iWssClientMessageExpander[k], connectionMeta: iWssConnectionMeta) => void | Promise<void> }
}) {
  /**
   * 基于http server实例初始化websocket
   * @author  韦胜健
   * @date    2023/10/17 10:28
   */
  const wsServer = new WebSocketServer(createConfig.wsParameters);

  /**
   * 处理websocket连接请求
   * @author  韦胜健
   * @date    2023/10/17 10:29
   */
  wsServer.on('request', function (request) {

    /*不允许的来源连接请求则拒绝连接*/
    if (!createConfig.originIsAllowed(request.origin)) {
      // Make sure we only accept requests from an allowed origin
      request.reject();
      logInfo((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
      return;
    }

    // const connection = request.accept('echo-protocol', request.origin);
    const connection = request.accept(request.requestedProtocols.join(','), request.origin);
    /*给这个连接生成一个随机的名字（唯一标识）*/
    const name = NamesHandler.get();
    /*立即给客户端发送serverInit消息，将这个申请的名字发送给客户端持有使用*/
    const serverInitMessage: iWssServerMessageMeta = { type: 'serverInit', data: { name } };
    /*本次连接的对象信息*/
    const connectionMeta: iWssConnectionMeta = { name, listener: [], connection, pause: false };
    /*吧serverInit消息发给客户端*/
    ConnectionHandler.sendUTF(connectionMeta, serverInitMessage);
    logInfo(`${name} 建立连接`);

    connection.on('message', function (message) {
      if (message.type === 'utf8') {
        /*处理客户端发来的消息*/
        const clientMessageMeta: iWssClientMessageMeta = JSON.parse(message.utf8Data);
        logInfo(`${name} 收到消息 ${clientMessageMeta.type}`);

        /*默认处理clientInit初始化的消息，将连接信息保存到ConnectionHandler中*/
        if (clientMessageMeta.type === 'clientInit') {
          connectionMeta.listener = clientMessageMeta.data.listener;
          ConnectionHandler.addConnection(connectionMeta);
        }
        /*其他消息处理动作*/
        if (!!createConfig.messageHandler[clientMessageMeta.type]) {
          createConfig.messageHandler[clientMessageMeta.type]!(clientMessageMeta.data as any, connectionMeta);
        } else {
          if (clientMessageMeta.type !== 'clientInit') {
            /*额外定义的，没有函数处理的消息*/
            logInfo(`${name} 无法处理的message.type=>${clientMessageMeta.type}`);
          }
        }
      } else if (message.type === 'binary') {
        /*暂时不处理二进制消息*/
        logInfo(`${name} 收到binary信息`);
        // console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
        // connection.sendBytes(message.binaryData);
      }
    });
    connection.on('close', function (reasonCode, description) {
      ConnectionHandler.removeConnection(connectionMeta);
      NamesHandler.release(connectionMeta.name);
      createConfig.messageHandler?.clientClose?.(connectionMeta, connectionMeta);
      logInfo(`${name} 断开连接：` + JSON.stringify({ reasonCode, description }));
    });
  });

  return {
    logInfo,
    getDatetime,
    ConnectionHandler,
    NamesHandler,
  };
}

/**
 * 获取当前时间，标准格式
 * @author  韦胜健
 * @date    2023/10/18 15:49
 */
const getDatetime = () => {
  const dateObj = new Date();
  return `${dateObj.getFullYear()}-${String(dateObj.getMonth() + 1).padStart(2, '0')}-${String(dateObj.getDate()).padStart(2, '0')} ${String(dateObj.getHours()).padStart(2, '0')}:${String(dateObj.getMinutes()).padStart(2, '0')}:${String(dateObj.getSeconds()).padStart(2, '0')}`;
};

/**
 * 带时间打印日志
 * @author  韦胜健
 * @date    2023/10/18 15:49
 */
const logInfo = (...args: any[]) => {
  console.log(`${getDatetime()}: `, ...args);
};

const ConnectionHandler = (() => {

  const state = {
    /**
     * 所有的连接信息
     * @author  韦胜健
     * @date    2023/10/17 10:30
     */
    connectionMetas: [] as iWssConnectionMeta[],
  };

  /**
   * 获取所有连接信息
   * @author  韦胜健
   * @date    2023/10/18 15:50
   */
  const getConnections = () => {return state.connectionMetas.map(i => ({ ...i }));};

  /**
   * 更新连接信息
   * @author  韦胜健
   * @date    2023/10/18 15:50
   */
  const updateConnections = (connections: iWssConnectionMeta[]) => {
    state.connectionMetas = connections;

    /*每次更新connectionMeta数据时，将新的数据信息发给监听conn类型的监听器*/
    const serverUpdateConnMessage: iWssServerMessageMeta = { type: 'serverUpdateConn', data: state.connectionMetas.map(i => ({ name: i.name, pause: i.pause, listeners: i.listener })) };

    state.connectionMetas.forEach(connectionMeta => {
      connectionMeta.listener.forEach(i => {
        i.type === 'conn' && sendUTF(connectionMeta, serverUpdateConnMessage);
      });
    });
  };

  /**
   * 发送消息
   * @author  韦胜健
   * @date    2023/10/18 15:50
   */
  const sendUTF = (connectionMeta: iWssConnectionMeta, message: iWssServerMessageMeta) => {!connectionMeta.pause && connectionMeta.connection.sendUTF(JSON.stringify(message));};

  /**
   * 添加一个连接信息
   * @author  韦胜健
   * @date    2023/10/18 15:50
   */
  const addConnection = (connectionMeta: iWssConnectionMeta) => {
    const allConnections = getConnections();
    allConnections.unshift(connectionMeta);
    updateConnections(allConnections);
  };

  /**
   * 移除一个连接信息
   * @author  韦胜健
   * @date    2023/10/18 15:50
   */
  const removeConnection = (connectionMeta: iWssConnectionMeta) => {
    const allConnections = getConnections();
    const index = allConnections.findIndex(i => i.name === connectionMeta.name);
    if (index > -1) {
      allConnections.splice(index, 1);
      updateConnections(allConnections);
    }
  };

  /**
   * 更新一个连接信息
   * @author  韦胜健
   * @date    2023/10/18 15:50
   */
  const updateConnection = (connectionMeta: iWssConnectionMeta) => {
    const allConnections = getConnections();
    const updateIndex = allConnections.findIndex(i => i.name === connectionMeta.name);
    if (updateIndex > -1) {
      allConnections[updateIndex] = connectionMeta;
      ConnectionHandler.updateConnections(allConnections);
    }
  };

  return {
    addConnection,
    getConnections,
    updateConnections,
    removeConnection,
    updateConnection,
    sendUTF,
  };
})();

/**
 * 每次来一个连接都给分配一个名字
 * @author  韦胜健
 * @date    2023/10/17 10:30
 */
const NamesHandler = (() => {

  /*可用的名字（唯一标识）*/
  const available: string[] = ["John", "Jane", "Michael", "Emma", "James", "Sarah", "David", "William", "Matthew", "Olivia", "Thomas", "Emily", "Christopher", "Hannah", "Robert", "Daniel", "Alexandra", "Ryan", "Michaela", "Ethan", "Nicholas", "Melissa", "Joshua", "Jessica", "Adam", "Kylie", "Bradley", "Hannah", "Jordan"];
  /*不可用的名字（已经被客户端持有的名字）*/
  const notAvailable: string[] = [];

  /**
   * 获取一个可用的名字
   * @author  韦胜健
   * @date    2023/10/18 15:51
   */
  const get = () => {
    const name = available.shift();
    if (!name) {
      throw new Error('名字已经用完啦！！！');
    }
    notAvailable.push(name);
    return name;
  };

  /**
   * 释放一个可用的名字
   * @author  韦胜健
   * @date    2023/10/18 15:51
   */
  const release = (name: string) => {
    const index = notAvailable.indexOf(name);
    if (index > -1) {
      notAvailable.splice(index, 1);
      available.push(name);
    }
  };

  return { get, release };
})();
