export type tWssDataType<T, K extends keyof T = keyof T> = K extends keyof T ? { type: K, data: T[K] } : never

/*@formatter:off*/
interface iWssConnectionListenerExpander {conn:{}}                                    // 客户端连接监听connection的变化
interface iWssConnectionListenerExpander {article:{articleId:string}}                 // 客户端连接监听文章的变化
/*客户端连接监听的消息类型*/
export type iWssConnectionListener = tWssDataType<iWssConnectionListenerExpander>
/*@formatter:on*/


/**
 * connection变化时通知的单个数据类型
 * @author  韦胜健
 * @date    2023/10/17 18:08
 */
export interface iWssServerUpdateConnMeta {
  name: string,
  pause: boolean,
  listeners: iWssConnectionListener[],
}

/*@formatter:off*/
export interface iWssServerMessageExpander {serverInit:{name:string}}
export interface iWssServerMessageExpander {serverUpdateConn:iWssServerUpdateConnMeta[]}
/*服务端给客户端发送的消息数据类型*/
export type iWssServerMessageMeta = tWssDataType<iWssServerMessageExpander>
/*@formatter:on*/

/*@formatter:off*/
export interface iWssClientMessageExpander {clientInit:{listener:iWssConnectionListener[]}}
export interface iWssClientMessageExpander {clientClose:{name: string,listener: iWssConnectionListener[]}}
/*客户端给服务端发送的消息数据类型*/
export type iWssClientMessageMeta = tWssDataType<iWssClientMessageExpander>
/*@formatter:on*/
