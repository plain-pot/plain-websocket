import {iWssClientMessageMeta, iWssServerMessageExpander, iWssServerMessageMeta} from "../utils/pws.type";

/**
 * 创建客户端websocket服务
 * @author  韦胜健
 * @date    2023/10/18 15:53
 */
export function createClientPws(createConfig: {
  origin?: string,
  /*websocket服务端口*/
  port: number,
  /*处理来自服务端的消息处理函数*/
  messageHandler: { [k in keyof iWssServerMessageExpander]?: (data: iWssServerMessageExpander[k]) => void | Promise<void> } & {
    onClose?: () => void,
    onError?: () => void,
    onOpen?: () => void,
  }
}) {

  const state = ({
    /*当前状态*/
    status: 'pending' as 'pending' | 'success' | 'failed',
    /*当前客户端连接标识*/
    data: null as null | { name: string }
  });

  const ws = new WebSocket(`ws://${createConfig.origin || 'localhost'}:${createConfig.port}`, 'echo-protocol');

  ws.onopen = (params) => {
    console.log('客户端连接成功', params);
    createConfig.messageHandler.onOpen?.();
  };

  ws.onmessage = (e) => {
    /*来自服务端的消息*/
    const serverMessageMeta: iWssServerMessageMeta = JSON.parse(e.data);

    /*默认处理服务端的serverInit消息*/
    if (serverMessageMeta.type === 'serverInit') {
      state.status = 'success';
      state.data = { name: serverMessageMeta.data.name };
    }

    /*处理其他消息*/
    if (!!createConfig.messageHandler[serverMessageMeta.type]) {
      createConfig.messageHandler[serverMessageMeta.type]!(serverMessageMeta.data as any);
    } else {
      if (serverMessageMeta.type !== 'serverInit')
        console.log(`无法处理的server message type:${(serverMessageMeta as any).type}`);
    }
  };

  ws.onclose = (e) => {
    console.log(`${state.data?.name} 关闭客户端连接`, e);
    createConfig.messageHandler.onClose?.();
  };

  ws.onerror = (e) => {
    console.log('客户端连接失败', e);
    createConfig.messageHandler.onError?.();
  };

  /**
   * 发送消息
   * @author  韦胜健
   * @date    2023/10/18 15:56
   */
  const send = (clientMessageMeta: iWssClientMessageMeta) => {
    ws.send(JSON.stringify(clientMessageMeta));
  };

  return { state, send, ws };
}

/**
 * 监听window派发的事件
 * @author  韦胜健
 * @date    2023/10/18 15:56
 */
export function addWindowListener<K extends keyof WindowEventMap>(type: K, listener: (this: Window, ev: WindowEventMap[K]) => any, options?: boolean | AddEventListenerOptions): (() => void) {
  window.addEventListener(type, listener, options);
  return () => {
    window.removeEventListener(type, listener);
  };
}
