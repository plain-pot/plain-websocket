# plain-websocket

- 本项目不能单独启动，也不能单独调试；
- 本项目是为了plain-editor实现协同编辑，基于websocket进行简易封装的websocket库，目的在于制定可扩展的类型以及api快速实现客户端与服务端之间的websocket通信；

# 使用示例：

## 1、扩展客户端、服务端发送的消息类型

src/pws.ts

```ts
import 'plain-websocket/utils/pws.type';

/**
 * 扩展服务端以及客户端发送的消息类型
 * @author  韦胜健
 * @date    2023/10/17 18:32
 */
declare module 'plain-websocket/utils/pws.type' {

    /*自定义添加的服务端发送消息类型*/

    /*@formatter:off*/
    export interface iWssServerMessageExpander {serverUpdateArticle: any;}
    /*@formatter:on*/

    /*自定义添加的客户端发送消息类型*/

    /*@formatter:off*/
    export interface iWssClientMessageExpander {clientUpdatePause: { name: string, pause: boolean };}
    export interface iWssClientMessageExpander {clientUpdateArticle: any;}
    /*@formatter:on*/
}

export default {};
```

## 2、服务端使用示例

server/server.websocket.ts

```ts
import http from "http";
import {createServerPss} from "plain-websocket/server/createServerPss";
import '../src/pws';

/**
 * 初始化http server
 * @author  韦胜健
 * @date    2023/10/17 10:28
 */
const server = http.createServer(function (request, response) {
  wss.logInfo('Received request for ' + request.url);
  response.writeHead(404);
  response.end();
});

/**
 * 监听4477端口请求
 * @author  韦胜健
 * @date    2023/10/17 10:28
 */
server.listen(4477, function () {wss.logInfo('Server is listening on port 4477');});

const wss = createServerPss({
  /**
   * 基于http server实例初始化websocket
   * @author  韦胜健
   * @date    2023/10/17 10:28
   */
  wsParameters: {
    httpServer: server, // You should not use autoAcceptConnections for production
    // applications, as it defeats all standard cross-origin protection
    // facilities built into the protocol and the browser.  You should
    // *always* verify the connection's origin and decide whether or not
    // to accept it.
    autoAcceptConnections: false
  },
  /**
   * 判断来源是否可信，是则响应websocket连接，否则拒绝websocket连接
   * @author  韦胜健
   * @date    2023/10/17 10:29
   */
  originIsAllowed: (origin) => {
    // put logic here to detect whether the specified origin is allowed.
    return true;
  },
  messageHandler: {
    clientUpdatePause: (data, ConnectionHandler) => {
      const connectionMeta = ConnectionHandler.getConnections().find(i => i.name === data.name);
      if (!connectionMeta) {return;}
      connectionMeta.pause = data.pause;
      ConnectionHandler.updateConnection(connectionMeta);
    },
  }
});
```

## 3、客户端使用示例

```tsx
import {designPage, onBeforeUnmount, reactive} from "plain-design-composition";
import {DisplayUsername} from "../components/DisplayUsername/DisplayUsername";
import {Table, Plc, Button} from 'plain-design';
import {addWindowListener, createClientPws} from "plain-websocket/client/createClientPws";
import {iWssClientMessageMeta, iWssServerUpdateConnMeta} from "plain-websocket/utils/pws.type";

export const TestWebSocket = designPage(() => {

    const state = reactive({
        connDataMetas: [] as iWssServerUpdateConnMeta[],
    });

    const wsc = createClientPws({
        port: 4477, messageHandler: {
            serverInit: () => {
                const clientMessageMeta: iWssClientMessageMeta = {type: 'clientInit', data: {listener: [{type: 'conn', data: {}}]}};
                wsc.send(clientMessageMeta);
            },
            serverUpdateConn: (data) => {
                state.connDataMetas = data;
            },
            serverUpdateArticle: () => {},
        }
    });

    onBeforeUnmount(
        addWindowListener('beforeunload', () => {wsc.ws.close();})
    );

    const handler = {
        /*切换链接暂停状态*/
        toggleConnectionPauseStatus: (connectionMeta: iWssServerUpdateConnMeta, pause?: boolean) => {
            const currentPause = state.connDataMetas.find(i => i.name === connectionMeta.name)!.pause;
            const clientMessageMeta: iWssClientMessageMeta = {type: "clientUpdatePause", data: {name: connectionMeta.name, pause: pause == undefined ? !currentPause : pause}};
            wsc.send(clientMessageMeta);
        },
    };

    return () => (
        !wsc.state.data?.name ? '连接中...' : (
            <div style={{padding: '0 16px'}}>
                <DisplayUsername name={wsc.state.data.name}/>
                <Table data={state.connDataMetas}>
                    <Plc title="用户名" field="name"/>
                    <Plc title="状态" field="pause" v-slots={{
                        normal: ({row}) => {
                            const meta: iWssServerUpdateConnMeta = row as any;
                            return !meta.pause ?
                                <Button size="small" label="暂停" status="primary" onClick={() => handler.toggleConnectionPauseStatus(meta)}/> :
                                <Button size="small" label="连接" status="secondary" onClick={() => handler.toggleConnectionPauseStatus(meta)}/>;
                        }
                    }}/>
                </Table>
            </div>
        )
    );
});
```

## 4、启动服务端的命令

```shell
"api": "ts-node --project node_modules/plain-websocket/server/server.tsconfig.json server/server.websocket.ts"
```
